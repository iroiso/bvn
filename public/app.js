
var BVNPattern = /^[0-9]{11}$/;
function checkBVN(){
  if((document.getElementById("bvn").value).match(BVNPattern))
  {
    //   alert(" Bvn Entered")
      document.getElementById("btn").style.opacity = 1;
      document.getElementById("error").style.visibility = "hidden";
      document.getElementById("btn").removeAttribute("disabled") ;
      
      
      
      
      
      
  }
  else{
    // alert("No Bvn Entered")
    
      document.getElementById("btn").style.opacity = 0.5;
      document.getElementById("error").style.visibility = "visible";
      
      
  }
}

function submitBVN(){
    event.preventDefault()
    document.getElementById("btn").style.visibility = "hidden";
    $("#slider").css("display", "block");
    
    

    $.ajax({
        url : 'https://api.paystack.co/bank/resolve_bvn/' + document.getElementById("bvn").value+ '/' ,
        contentType : "application/json; charset=utf-8",
        dataType : "json",
        headers: {
            'Authorization' : 'Bearer sk_live_ccd3e5b46eedff726a3bded885a8ddb41b52bc3d' 
        },
        success: function (data) {
            console.log(data)            
            var bvnData = data.data    
            console.log(bvnData.dob)       
            if(data.status == true && data.message == "BVN resolved" && typeof(bvnData.dob) !='undefined') {
                $("#slider").css("display", "none");
                $("#btn_again").css("display", "block");
                
                $("#bvnError").css("display", "none");
                
                $("#bvnDetails").css("display", "block");
    

            $("#dob").text(bvnData.dob)
            $("#firstname").text(bvnData.first_name)
            $("#lastname").text(bvnData.last_name)
            $("#phone").text(bvnData.mobile)
            $("#bvnnum").text(bvnData.bvn)
            }
            else{
            $("#bvnError").css("display", "block");
            $("#bvnDetails").css("display", "none");
            
            
            }

      }
    })

}

